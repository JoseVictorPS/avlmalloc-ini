output: mod_random.o AVL.o
	gcc -std=c99 mod_random.o AVL.o -o output

mod_random.o: mod_random.c
	gcc -std=c99 -c mod_random.c

AVL.o: AVL.c AVL.h
	gcc -std=c99 -c AVL.c

clean:
	rm *.o output

#target: dependencies
#	action
