#include "AVL.h"
#include <time.h>

//make && clean
//./output

int main(void) {

    clock_t comeco = clock();

    printf("COMECOU\n");
    AVL*raiz = random_tree(25000); // Preenche a árvore com valores aleatórios

    int qtd = node_counter(raiz);
    printf("NODES ALOCADOS: %d\n", qtd);
    printf("A MEMORIA GASTA NA HEAP EM BYTES: %d\n", qtd * sizeof(AVL));

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    printf("TERMINOU\n");

    clock_t fim = clock();

    double tempo_gasto = (double)(fim - comeco) / CLOCKS_PER_SEC;
    printf("\nO TEMPO GASTO: %lf SEGUNDOS\n\n", tempo_gasto);

    return 0;
}
