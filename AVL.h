#ifndef AVL_H
#define AVL_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct arv { // Nó de cada árvore
    int info; // Informação de cada nó
    struct arv * esq; // Ponteiro para o filho a esquerda
    struct arv * dir; // Ponteiro para o filho a direita
}AVL;

AVL * initialize();
AVL * gen_leaf(int c);
int empty_tree(AVL * a);
AVL * free_tree(AVL * a);
AVL* build_tree(AVL * r);
AVL * insert_tree(AVL * r, int v);
void print_tree(AVL*r);
void call_print_tree(AVL*r);
int read_value();
AVL*UX_tree();
int tree_height(AVL * r);
int max(int a, int b);
int balance_coef(AVL * r);
AVL * balance(AVL * r);
AVL * rse(AVL * r);
AVL * rsd(AVL * r);
AVL * rde(AVL * r);
AVL * rdd(AVL * r);
AVL * random_tree();
AVL * remove_node(AVL*r, int v);
AVL * free_node(AVL * r);
AVL * UX_remove(AVL*r);
int node_counter(AVL * tree);

#endif //AVL_H
